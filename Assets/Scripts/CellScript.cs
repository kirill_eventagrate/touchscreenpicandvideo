﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;

public class CellScript : MonoBehaviour {

    public GameObject Image;
    public GameObject Video;
    public FileInfo file;

    Vector3 startPos;
    

    // Use this for initialization
    void Start () {
        Image.GetComponent<Button>().onClick.AddListener(() => ShowBigPicture());
        Video.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(() => PlayVideo());
        startPos = transform.position;
    }
	
	// Update is called once per frame
	void Update () {
        
    }

    public void ShowImage(FileInfo _file)
    {
        file = _file;
        Image.SetActive(true);
        Video.SetActive(false);
        SetImageFromFile(file.ToString());
    }
    public void ShowVideo(FileInfo _file)
    {
        file = _file;
        Video.SetActive(true);
        Image.SetActive(false);
        SetVideoFromFile(file.ToString());
    }

    public void GoToStartPosition()
    {
        transform.GetChild(0).GetComponent<Canvas>().sortingOrder = 0;
        iTween.MoveTo(gameObject, iTween.Hash("position", startPos, "time", 0.1f, "easetype", iTween.EaseType.easeOutSine));
    }

    void SetImageFromFile(string file)
    {
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            Image.GetComponent<Image>().sprite = mySprite;
            Image.GetComponent<Image>().preserveAspect = true;
        }
    }

    void SetVideoFromFile(string file)
    {
        RenderTexture videoTexture = new RenderTexture(960, 540, 24, RenderTextureFormat.ARGB32);
        videoTexture.Release();
        Video.GetComponent<RawImage>().texture = videoTexture;
        Video.GetComponent<VideoPlayer>().targetTexture = videoTexture;
        Video.GetComponent<VideoPlayer>().url = file;
        Video.GetComponent<VideoPlayer>().prepareCompleted += GetVideoPreview;
        Video.GetComponent<VideoPlayer>().time = 10;
        Video.GetComponent<VideoPlayer>().Prepare();
    }

    void GetVideoPreview(VideoPlayer _videoPlayer)
    {
        _videoPlayer.Play();
        Loom.QueueOnMainThread(() => {
            _videoPlayer.Pause();
        }, 0.1f);
    }

    public void HideContent()
    {
        Image.SetActive(false);
        Video.SetActive(false);
    }

    public void PlayVideo()
    {
        //Debug.Log("PlayVideo");
        MainScript.instance.BigVideoContainer.SetActive(true);
        BigVideoManager.instance.Init(file);
    }

    void ShowBigPicture()
    {
        MainScript.instance.BigPicContainer.SetActive(true);
        BigImageManager.instance.Init(file);
    }
}
