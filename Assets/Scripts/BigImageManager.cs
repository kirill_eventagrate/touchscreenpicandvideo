﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class BigImageManager : MonoBehaviour {

    static public BigImageManager instance = null;

    public Image BigImg;
    public Text ImageText;
    public Button LeftBtn;
    public Button RightBtn;
    public Button CloseBtn;


    List<FileInfo> picList = new List<FileInfo>();
    int correctIndex;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Use this for initialization
    void Start () {
        /*LeftBtn.onClick.AddListener(() => ChangeImage("prev"));
        RightBtn.onClick.AddListener(() => ChangeImage("next"));*/
        CloseBtn.onClick.AddListener(() => CloseBigPic());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init(FileInfo _file)
    {
        MainScript.SetImageFromFile(BigImg.gameObject, _file.ToString());
        ImageText.text = _file.Name;
    }

    void ChangeImage(string type)
    {
        if ((type == "next") && ((correctIndex + 1) < picList.Count))
        {
            correctIndex++;
        }else if ((type == "prev") && ((correctIndex - 1) >= 0))
        {
            correctIndex--;
        }
        SetImage();
    }

    void SetImage()
    {
        MainScript.SetImageFromFile(BigImg.gameObject, picList[correctIndex].ToString());
        ImageText.text = picList[correctIndex].Name;
    }

    void CloseBigPic()
    {
        gameObject.SetActive(false);
    }
}
