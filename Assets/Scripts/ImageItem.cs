﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ImageItem : MonoBehaviour {

    Text title;

    string Name;
    string Path;
    int Index;

    Button btn;
    VideoPlayer videoPlayer;

	// Use this for initialization
	void Start () {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(() => ShowBigPic());
        

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetParams(string _name, string _path, int _index)
    {
        Name = _name;
        Path = _path;
        Index = _index;
        if (transform.GetChild(1).GetComponent<Text>())
        {
            title = transform.GetChild(1).GetComponent<Text>();
            title.text = Name.Split('.')[0];
        }
    }

    public void SetupVideoPrev()
    {
        videoPlayer = transform.GetChild(0).GetComponent<VideoPlayer>();
        Vector2 size = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta;
        RenderTexture videoTexture = new RenderTexture(Convert.ToInt32(size.x), Convert.ToInt32(size.y), 24, RenderTextureFormat.ARGB32);
        videoTexture.Release();
        transform.GetChild(0).GetComponent<RawImage>().texture = videoTexture;
        videoPlayer.targetTexture = videoTexture;
        videoPlayer.url = Path;
        videoPlayer.time = 10;
        videoPlayer.prepareCompleted += GetVideoPreview;
        videoPlayer.Prepare();
    }

    void GetVideoPreview(VideoPlayer _videoPlayer)
    {
        _videoPlayer.Play();
        Loom.QueueOnMainThread(() => {
            _videoPlayer.Pause();
        }, 0.1f);
    }

    public string GetName() { return Name; }
    public string GetPath() { return Path; }

    void ShowBigPic()
    {
        StartCoroutine(Show(Index - MainScript.instance.GetMainCellImageIndex()));
    }

    IEnumerator Show(int count)
    {
        float time = 0.06f;
        if (count > 0)
        {
            for (float i = 0; i < count; i++)
            {
                MainScript.instance.SwitchCarouselToRight(time);
                yield return new WaitForSeconds(time - 0.02f);
            }
        } else
        {
            count *= (-1);
            for (float i = 0; i < count; i++)
            {
                MainScript.instance.SwitchCarouselToLeft(time);
                yield return new WaitForSeconds(time - 0.02f);
            }
        }
    }
}
