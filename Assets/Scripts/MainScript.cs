﻿using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MainScript : MonoBehaviour {

    static public MainScript instance = null;

    public string BackgroundFolder;
    public string ContentFolder = "ContentFolder";

    public GameObject BackgroundImage;

    public GameObject RowPrefab;
    public GameObject picItem;
    public GameObject videoItem;
    public GameObject cellPrefab;
    
    public GameObject PicContencContainer;
    

    public GameObject BigPicContainer;
    public GameObject BigVideoContainer;

    int itemCount = 8;
    int imageIndex = 0;
    GameObject row;
    
    List<CellData> cellDatas = new List<CellData>();
    List<CellData> cellContent = new List<CellData>();
    public List<FileInfo> picList = new List<FileInfo>();
    public List<FileInfo> videoList = new List<FileInfo>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Use this for initialization
    void Start () {
        if (!Directory.Exists("ContentFolder"))
        {
            Directory.CreateDirectory("ContentFolder");
        }
        ContentFolder = "ContentFolder";

        Init();
        //Resources.UnloadUnusedAssets();
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.E))
        {
            SwitchCarouselToRight(1f);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            SwitchCarouselToLeft(1f);
        }
    }

    void Init()
    {
        SetImageFromFile(BackgroundImage, "background.jpg");
        Loom.QueueOnMainThread(() => {
            ShowPicturies();
        });
    }

    void ShowPicturies()
    {
        picList.Clear();
        picList = GetFileList(ContentFolder);
        if (picList.Count > 0)
        {
            videoList.Clear();
            Loom.QueueOnMainThread(() => {
                PicContencContainer.SetActive(true);
                StartCoroutine(SetImageArray(PicContencContainer.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject, picList));

            }, 1f);
        }
    }

    void InitCarousel()
    {
        Vector3 startPos = new Vector3(0, 0, 2);
        int i = 0;
        foreach (FileInfo file in picList)
        {
            GameObject cellObj = Instantiate(cellPrefab, startPos, Quaternion.identity);
            cellContent.Add(new CellData(cellObj.transform.position, cellObj, file, i));
            startPos = new Vector3(startPos.x, startPos.y, startPos.z + 0.2f);
            i++;
        }
        Debug.Log("InitCarousel");
        StartCoroutine(InitCellContent());
    }

    IEnumerator InitCellContent()
    {
        foreach (CellData cell in cellContent)
        {
            cell.SetContent();
            yield return new WaitForSeconds(0.15f);
        }
        GameObject[] Cells = GameObject.FindGameObjectsWithTag("Cell").OrderBy(go => go.name).ToArray();
        int i = 0;
        foreach (GameObject cell in Cells)
        {
            CellData newCell = new CellData(cell.transform.position, cellContent[i].GetCellTarget(), i, 0.5f);
            newCell.SetFirstOrder();
            cellDatas.Add(newCell);
            i++;
        }
    }

    public void SwitchCarouselToRight(float speed)
    {
        if(GetMainCellImageIndex() < picList.Count - 1)
        {
            int first = 0;
            int last = cellDatas.Count - 1;
            if (cellDatas[first].GetCellTarget() != null)
            {
                cellDatas[first].GetCellTarget().GetComponent<CellScript>().GoToStartPosition();
            }
            for (int i = 1; i < cellDatas.Count; i++)
            {
                cellDatas[i - 1].SetCellTarget(cellDatas[i].GetCellTarget(), speed, cellDatas[i].GetImageIndex());
            }
            int correctIndex = imageIndex + cellDatas.Count;
            if ((correctIndex) < picList.Count) {
                cellDatas[last].SetCellTarget(cellContent[correctIndex].GetCellTarget(), 0.1f, cellContent[correctIndex].GetImageIndex());
                cellDatas[last].SetFirstOrder();
            }
            else
            {
                cellDatas[last].ResetCellTarget();
            }
            cellDatas[last].SetIndex(correctIndex);
            imageIndex++;
            //Resources.UnloadUnusedAssets();
        }
    }

    public void SwitchCarouselToLeft(float speed)
    {
        if(GetMainCellImageIndex() > 0)
        {
            int first = 0;
            int last = cellDatas.Count - 1;
            if (cellDatas[last].GetCellTarget() != null)
            {
                cellDatas[last].GetCellTarget().GetComponent<CellScript>().GoToStartPosition();
            }
            for (int i = cellDatas.Count - 2; i >= 0; i--)
            {
                cellDatas[i + 1].SetCellTarget(cellDatas[i].GetCellTarget(), speed, cellDatas[i].GetImageIndex());
            }
            int correctIndex = imageIndex - 1;
            imageIndex--;
            if (correctIndex >= 0)
            {
                cellDatas[first].SetCellTarget(cellContent[correctIndex].GetCellTarget(), 0.1f, cellContent[correctIndex].GetImageIndex());
                cellDatas[first].SetFirstOrder();
            }
            else
            {
                cellDatas[first].ResetCellTarget();
            }
            cellDatas[first].SetIndex(imageIndex);
            //Resources.UnloadUnusedAssets();
        }
    }

    public List<FileInfo> GetFileList(string path)
    {
        List<FileInfo> resultArray = new List<FileInfo>();
        if (Directory.Exists(path))
        {
            var info = new DirectoryInfo(path);
            FileInfo[] fileList;
            fileList = info.GetFiles("*.*");
            foreach (FileInfo file in fileList)
            {
                if (file.Extension.ToLower() == ".png" || file.Extension.ToLower() == ".jpg" || file.Extension.ToLower() == ".mp4")
                {
                    resultArray.Add(file);
                }
                
            }
        }
        return resultArray;
    }

    public int GetMainCellImageIndex()
    {
        return cellDatas[3].GetImageIndex();
    }

    



    IEnumerator SetImageArray(GameObject parent, List<FileInfo> files)
    {
        for (int i = 0; i < parent.transform.childCount; i++)
        {
            Destroy(parent.transform.GetChild(i).gameObject);
        }
        for (int i = 0; i < files.Count; i++)
        {
            float width = Screen.width / itemCount;
            float height = width / (16f / 9f);
            if (files[i].Extension.ToLower() == ".png" || files[i].Extension.ToLower() == ".jpg")
            {
                //picItem.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
                GameObject img = Instantiate(picItem, Vector3.zero, Quaternion.identity);
                img.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
                img.transform.SetParent(parent.transform);
                img.GetComponent<ImageItem>().SetParams(files[i].Name, files[i].ToString(), i);
                SetImageFromFile(img.transform.GetChild(0).gameObject, files[i].ToString());
                yield return new WaitForSeconds(0.1f);
            }
            else if (files[i].Extension.ToLower() == ".mp4")
            {
                //videoItem.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
                GameObject video = Instantiate(videoItem, Vector3.zero, Quaternion.identity);
                video.transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(width, height);
                video.transform.SetParent(parent.transform);
                video.GetComponent<ImageItem>().SetParams(files[i].Name, files[i].ToString(), i);
                video.GetComponent<ImageItem>().SetupVideoPrev();
                yield return new WaitForSeconds(0.3f);
            }
            else
            {
                yield return new WaitForSeconds(0.1f);
            }
        }
        InitCarousel();
    }

    static public void SetImageFromFile(GameObject img, string file)
    {
        Texture2D tex = null;
        byte[] fileData;
        string filePath = file;
        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            Sprite mySprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            img.GetComponent<Image>().sprite = mySprite;
            img.GetComponent<Image>().preserveAspect = true;
        }
    }
}

class CellData
{
    Vector3 Position;
    GameObject CellTarget;
    int imageIndex;
    FileInfo file;
    public CellData(Vector3 _pos, GameObject _cell, int _imageIndex, float _time)
    {
        Position = _pos;
        CellTarget = _cell;
        imageIndex = _imageIndex;
        iTween.MoveTo(CellTarget, iTween.Hash("position", Position, "time", _time, "easetype", iTween.EaseType.easeOutSine));
    }
    public CellData(Vector3 _pos, GameObject _cell, FileInfo _file, int _imageIndex) {
        Position = _pos;
        CellTarget = _cell;
        file = _file;
        imageIndex = _imageIndex;
        //SetContent(_file, _imageIndex);
    }
    public Vector3 GetPosition() { return Position; }
    public GameObject GetCellTarget() { return CellTarget; }
    public int GetImageIndex() { return imageIndex; }
    public void SetCellTarget(GameObject _newCellTarget, float _time, int _imageIndex) {
        imageIndex = _imageIndex;
        CellTarget = _newCellTarget;
        if (CellTarget != null)
        {
            iTween.MoveTo(CellTarget, iTween.Hash("position", Position, "time", _time, "easetype", iTween.EaseType.easeOutSine));
        }
    }
    public void SetContent()
    {
        //imageIndex = _imageIndex;
        if (file.Extension.ToLower() == ".png" || file.Extension.ToLower() == ".jpg")
        {
            CellTarget.GetComponent<CellScript>().ShowImage(file);
        }
        else if (file.Extension.ToLower() == ".mp4")
        {
            CellTarget.GetComponent<CellScript>().ShowVideo(file);
        }
    }
    public void SetIndex(int _imageIndex) { imageIndex = _imageIndex; }
    public void SetFirstOrder() { CellTarget.transform.GetChild(0).GetComponent<Canvas>().sortingOrder = 1; }
    public void ResetCellTarget() { CellTarget = null; }
}
