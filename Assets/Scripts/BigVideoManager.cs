﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class BigVideoManager : MonoBehaviour {

    static public BigVideoManager instance = null;

    public VideoPlayer BigVideo;
    public Text VideoText;
    public Button CloseBtn;
    public Button ControllVideoBtn;
    public Slider slider;
    public Sprite playSprite;
    public Sprite pauseSprite;





    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    // Use this for initialization
    void Start () {
        CloseBtn.onClick.AddListener(() => CloseBigPic());
        ControllVideoBtn.onClick.AddListener(() => PlayPauseVideo());
        slider.minValue = 0;
        BigVideo.prepareCompleted += PreapreVideoParams;
        BigVideo.Prepare();
    }
	
	// Update is called once per frame
	void Update () {
        if (BigVideo.isPlaying)
        {
            slider.value = (float)BigVideo.time;
        }
        
    }

    public void MoveSlider()
    {
        BigVideo.time = slider.value;
    }

    void PreapreVideoParams(VideoPlayer _videoPlayer)
    {
        slider.maxValue = CalculateLengh();
        ControllVideoBtn.gameObject.GetComponent<Image>().sprite = pauseSprite;
    }

    float CalculateLengh()
    {
        float fraction = (float)BigVideo.frameCount / (float)BigVideo.frameRate;
        return fraction;
    }

    public void PlayPauseVideo()
    {
        if (BigVideo.isPlaying)
        {
            BigVideo.Pause();
            ControllVideoBtn.gameObject.GetComponent<Image>().sprite = playSprite;
        }else
        {
            BigVideo.Play();
            ControllVideoBtn.gameObject.GetComponent<Image>().sprite = pauseSprite;
        }
    }

    public void Init(FileInfo _file)
    {
        BigVideo.targetTexture.Release();
        BigVideo.url = _file.ToString();
        BigVideo.Prepare();
        VideoText.text = _file.Name;
    }

    void CloseBigPic()
    {
        gameObject.SetActive(false);
    }
}
